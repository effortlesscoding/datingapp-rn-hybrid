import React from 'react'
import {Button, Text, View,} from 'react-native'

const LoginScene = (props) => {

  const _handleOnLogin = () => {
    // go to singles review scene
    props.navigator.push({ key: 'singlesreview', index: 1})
  }

  return (
    <View>
      <Text>Login Scene</Text>
      <Button
        onPress={_handleOnLogin}
        title={"Press to login"}
      />
    </View>
  )
}

export {
  LoginScene,
}
