import React from 'react'
import {Button, Text, View,} from 'react-native'

const SinglesReviewScene = (props) => {
  const _handleBack = () => {
    props.navigator.pop()
  }
  
  return (
    <View>
      <Text>Singles Review</Text>
      <Button
        onPress={_handleBack}
        title={'Pop'}
      />
    </View>
  )
}

export {
  SinglesReviewScene,
}
