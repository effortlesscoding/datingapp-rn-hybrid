import React, { Component, } from 'react';
import {Navigator, View, Text} from 'react-native';
import {
  LoginScene,
  SinglesReviewScene,
} from './scenes'

class App extends Component {

  constructor(props) {
    super(props)
  }

  renderNavigationBar = () => {
    return (
     <Navigator.NavigationBar
       routeMapper={{
         LeftButton: (route, navigator, index, navState) =>
          { return (<Text>Cancel</Text>); },
         RightButton: (route, navigator, index, navState) =>
           { return (<Text>Done</Text>); },
         Title: (route, navigator, index, navState) =>
           { return (<Text>Awesome Nav Bar</Text>); },
       }}
       style={{backgroundColor: 'gray'}}
     />
    )
  }

  render() {
    const routes = [
      {key: 'login', index: 0},
      {key: 'singlesreview', index: 1},
    ];
    return (
      <Navigator
        initialRoute={routes[0]}
        initialRouteStack={routes}
        renderScene={this._handleRenderScene}
        navigationBar={this.renderNavigationBar()}
        configureScene={(route, routeStack) => Navigator.SceneConfigs.SwipeFromLeft}
        style={{ padding: 40 }}
      />
    )
  }

  _handleRenderScene = (route, navigator) => {
    switch (route.key) {
      case 'login': {
        return (
          <LoginScene
            navigator={navigator}
          />
        )
      }
      case 'singlesreview': {
        return (
          <SinglesReviewScene
            navigator={navigator}
          />
        )
      }
      default: {
        return (
          <Text>Hello {route.title}</Text>
        )
      }
    }
  }
}

export {
  App,
};
